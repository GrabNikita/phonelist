(function ($) {
    
    $(document).ready(function () {
       
        $(document).on("click",".show-characteristics",function (e) {
           
            var $this = $(e.target);
            
            var $productWrapper = $this.closest(".phone-wrapper");
            if ($productWrapper.length <= 0) {
                return;
            }
            
            var productId = $productWrapper.data("id");
            if (productId === undefined || productId === 0) {
                return;
            }
            
            var $characteristicsWrapper = $productWrapper.find(".characteristics-wrapper");
            if ($characteristicsWrapper.length <= 0) {
                return;
            }
            
            if ($characteristicsWrapper.find("*").length > 0) {
                
                $characteristicsWrapper.slideToggle();
                return;
            }
            
            $.ajax(
                "/ajax/getProductCharacteristics/" + productId,
                {
                    dataType: "html",
                    success: function (response) {
                        
                        response = response.trim();

                        var elem = document.createElement('textarea');
                        elem.innerHTML = response;
                        response = elem.value;
                        
                        response = JSON.parse(response);
                        
                        response.forEach(function (item, index) {
                            
                            var $charItem = $(
                                "<div class='characteristic-wrapper row'>" +
                                    "<div class='char-name col-xs-6'>" +
                                        item.name +
                                    "</div>" +
                                    "<div class='char-value col-xs-6'>" +
                                        item.value +
                                    "</div>" +
                                "</div>"
                            );
                            
                            $characteristicsWrapper.append($charItem);
                        });
                        
                        $characteristicsWrapper.slideDown();
                    }
                }
            )
        });
    });
})(jQuery);
