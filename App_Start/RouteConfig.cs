﻿using System.Web.Mvc;
using System.Web.Routing;

namespace PhonesList
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               "Ajax",
                "ajax/{action}/{id}/",
                new {Controller = "Ajax", action = "Index", id = UrlParameter.Optional}
           );
            
            routes.MapRoute(
                "Default",
                "{action}/",
                new {controller = "Home", action = "Index"}
            );
        }
    }
}