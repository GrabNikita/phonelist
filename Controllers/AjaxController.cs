﻿using System.Web.Mvc;
using PhonesList.Models.Caracteristics;

namespace PhonesList.Controllers
{
    public class AjaxController : Controller
    {
        public ActionResult getProductCharacteristics(int id)
        {
            
            CCharacteristicList charList = new CCharacteristicList();
            
            charList.setProductId(id);
            
            charList.getListFromDB();

            ViewBag.charList = charList.getList(); 
            
            return View();
        }
    }
}