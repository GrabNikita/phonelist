﻿using System.Web.Mvc;
using PhonesList.Models.Products.Phones;

namespace PhonesList.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
           
            CPhoneList phoneList = new CPhoneList();

            phoneList.getListFromDB(new {sort = "ASC"});

            ViewBag.phoneList = phoneList.getList();
            
            return View();
        }

        public ActionResult New()
        {
            
            CPhoneList phoneList = new CPhoneList();

            phoneList.getListFromDB(new {realese_date = "DESC"});

            ViewBag.phoneList = phoneList.getList();

            return View();
        }

        public ActionResult Popular()
        {
            
            CPhoneList phoneList = new CPhoneList();

            phoneList.getListFromDB(new {rating = "DESC"});

            ViewBag.phoneList = phoneList.getList();

            return View();
        }
    }
}