﻿namespace PhonesList.Models.Caracteristics
{
    public interface ICharacteristic
    {

        string getName();

        string getValue();

        int getProductId();
    }
}