﻿using System.Runtime.Serialization;

namespace PhonesList.Models.Caracteristics
{
    public class CCharacteristic : ICharacteristic, ISerializable
    {

        private string name;
        private string value;
        private int productId;
        
        public string getName()
        {
            return name;
        }

        public string getValue()
        {
            return value;
        }

        public int getProductId()
        {
            return productId;
        }

        public void setName(string name)
        {
            this.name = name;
        }
        

        public void setValue(string value)
        {
            this.value = value;
        }

        public void setProductId(int productId)
        {
            this.productId = productId;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("name", name, typeof(string));
            info.AddValue("value", value, typeof(string));
            info.AddValue("productId", productId, typeof(string));
        }
    }
}