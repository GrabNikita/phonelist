﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using PhonesList.Models.DB;

namespace PhonesList.Models.Caracteristics
{
	public class CCharacteristicList : ICharacteristicList
	{
		private int productId;
		private List<ICharacteristic> characteristicList;

		public CCharacteristicList()
		{
			characteristicList = new List<ICharacteristic>();
		}

		public int getProductId()
		{
			return productId;
		}

		public void setProductId(int productId)
		{
			this.productId = productId;
		}

		public void getListFromDB()
		{
			SqlConnection connection = CDBConnection.getInstance().getConnection();

			string sqlQuery =
				"SELECT CharacteristicTypes.name, CharacteristicValues.value, Products.product_id " +
				"FROM CharacteristicValues " +
				"INNER JOIN CharacteristicTypes ON CharacteristicValues.char_type_id = CharacteristicTypes.char_type_id " +
				"INNER JOIN Products ON CharacteristicValues.product_id = Products.product_id " +
				"WHERE CharacteristicValues.product_id = " + productId;

			SqlCommand command = new SqlCommand(sqlQuery, connection);

			try
			{
				SqlDataReader dataReader = command.ExecuteReader();

				while (dataReader.Read())
				{
					var characteristicRecord = (IDataRecord) dataReader;
					CCharacteristic characteristic = new CCharacteristic();

					characteristic.setName(Convert.ToString(characteristicRecord[0]));

					characteristic.setValue(Convert.ToString(characteristicRecord[1]));

					characteristic.setProductId(Convert.ToInt32(characteristicRecord[2]));

					characteristicList.Add(characteristic);
				}

				dataReader.Close();
			}
			catch (InvalidOperationException e)
			{
				Console.WriteLine(e);
				//throw;
			}
		}

		public List<ICharacteristic> getList()
		{
			return characteristicList;
		}
	}
}