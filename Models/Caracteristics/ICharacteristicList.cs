﻿using System.Collections.Generic;

namespace PhonesList.Models.Caracteristics
{
    public interface ICharacteristicList
    {

        int getProductId();
        
        void setProductId(int productId);
        
        void getListFromDB();

        List<ICharacteristic> getList();
    }
}