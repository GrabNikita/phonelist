﻿using System.Data.SqlClient;

namespace PhonesList.Models.DB
{
    public interface IDBConnection
    {
        SqlConnection getConnection();
    }
}