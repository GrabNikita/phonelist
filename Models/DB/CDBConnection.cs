﻿using System.Configuration;
using System.Data.SqlClient;

namespace PhonesList.Models.DB
{
    public class CDBConnection : IDBConnection
    {
        private SqlConnection connection;

        private static IDBConnection instance;

        private CDBConnection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["PhonesListDB"].ToString();
            
            connection = new SqlConnection(connectionString);
            connection.Open();
              
        }

        public static IDBConnection getInstance()
        {
            if (instance == null)
            {
                instance = new CDBConnection();
            }
            return instance;
        }
        public SqlConnection getConnection()
        {
            return connection;
        }
    }

}