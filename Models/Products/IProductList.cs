﻿using System;
using System.Collections.Generic;

namespace PhonesList.Models.Products
{
    public interface IProductList
    {

        List<IProduct> getList();

        void getListFromDB(Object sort = null);
    }
}