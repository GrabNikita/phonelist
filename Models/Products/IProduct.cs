﻿using System;

namespace PhonesList.Models.Products
{
    public interface IProduct
    {
        int getId();
        string getName();
        float getPrice();
        int getBrandId();
        string getBrandName();
        int getModelId();
        string getModelName();
        string getImageSrc();
        string getRealeseDateString();
        DateTime getRealeseDate();
        int getSortIndex();
        int getRating();
        string getDescription();
    }
}