﻿using System;
using System.Runtime.Serialization;

namespace PhonesList.Models.Products.Phones
{
    public class CPhone : IProduct, ISerializable
    {
        private int id;
        private string name;
        private float price;
        private int brandId;
        private string brandName;
        private int modelId;
        private string modelName;
        private string imageSrc;
        private DateTime realeseDate;
        private int sortIndex;
        private int rating;
        private string description;

        public CPhone()
        {

        }

        public int getId()
        {
            return id;
        }

        public string getName()
        {
            return name;
        }

        public float getPrice()
        {
            return price;
        }

       

        public void setName(string name)
        {
            this.name = name;
        }
        
        public int getBrandId()
        {
            return brandId;
        }
        
        public string getBrandName()
        {
            return brandName;
        }

        public void setBrand(int brandId, string brandName)
        {
            this.brandId = brandId;
            this.brandName = brandName;
        }
        
        public int getModelId()
        {
            return modelId;
        }

        public string getModelName()
        {
            return modelName;
        }

        public void setModel(int modelId, string modelName)
        {
            this.modelId = modelId;
            this.modelName = modelName;
        }

        public string getImageSrc()
        {
            return imageSrc;
        }

        public void setImageSrc(string imageSrc)
        {
            this.imageSrc = imageSrc;
        }

        public string getRealeseDateString()
        {
            return realeseDate.ToString("MM.yyyy");
        }

        public DateTime getRealeseDate()
        {
            return realeseDate;
        }

        public void setRealeseDate(DateTime realeseDate)
        {
            this.realeseDate = realeseDate;
        }

        public int getSortIndex()
        {
            return sortIndex;
        }

        public void setSortIndex(int sortIndex)
        {
            this.sortIndex = sortIndex;
        }

        public int getRating()
        {
            return rating;
        }

        public string getDescription()
        {
            return description;
        }

        public void setDescription(string description)
        {
            this.description = description;
        }

        public void setRating(int rating)
        {
            this.rating = rating;
        }

        public void SetPrice(float price)
        {
            this.price = price;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("name", name, typeof(string));
        }
    }
}