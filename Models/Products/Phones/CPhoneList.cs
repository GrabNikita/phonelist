﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using PhonesList.Models.DB;

namespace PhonesList.Models.Products.Phones
{
	public class CPhoneList : IProductList
	{
		
		public CPhoneList()
		{
			this.productList = new List<IProduct>();
		}

		private List<IProduct> productList;

		public List<IProduct> getList()
		{
			return productList;
		}

		public void getListFromDB(Object sort = null)
		{
			SqlConnection connection = CDBConnection.getInstance().getConnection();

			string sqlQuery =
				"SELECT " +
					"Products.product_id, Products.name, Products.price, Products.description, " +
					"Models.model_id,Models.name, " +
					"Brands.brand_id, Brands.name, " +
					"Products.realese_date, Products.image, Products.rating, Products.sort " +
				"FROM Products " +
				"INNER JOIN Models ON Products.model_id = Models.model_id " +
				"INNER JOIN Brands ON Models.brand_id = Brands.brand_id";
			
			if (sort != null)
			{

				sqlQuery += " ORDER BY ";
				
				var propList = sort.GetType().GetProperties();
				
				var i=0;
				var count =propList.Length;
				
				foreach (PropertyInfo propertyInfo in propList)
				{
					var property = sort.GetType().GetProperty(propertyInfo.Name);
					if (property != null)
					{
						var value = property.GetValue(sort, null);
						sqlQuery += " " + propertyInfo.Name + " " + value;
					}

					if (++i < count)
					{
						sqlQuery += ",";
					}
				}
			}
			Console.WriteLine(sqlQuery);
			
			SqlCommand command = new SqlCommand(sqlQuery, connection);

			try
			{
				SqlDataReader dataReader = command.ExecuteReader();
				
				while (dataReader.Read())
				{
					var phoneRecord = (IDataRecord) dataReader;
					CPhone phone = new CPhone();
					
					phone.setId(Convert.ToInt32(phoneRecord[0]));
					phone.setName((string) phoneRecord[1]);

					phone.setBrand(
						Convert.ToInt32(phoneRecord[6]), 
						Convert.ToString(phoneRecord[7])
						);
					phone.setModel(
						Convert.ToInt32(phoneRecord[4]), 
						Convert.ToString(phoneRecord[5])
						);

					phone.setDescription(Convert.ToString(phoneRecord[3]));
					phone.setImageSrc(Convert.ToString(phoneRecord[9]));

					phone.setRating(Convert.ToInt32(phoneRecord[10]));
					phone.setSortIndex(Convert.ToInt32(phoneRecord[11]));

					phone.SetPrice(Convert.ToSingle(phoneRecord[2]));

					phone.setRealeseDate(Convert.ToDateTime(phoneRecord[8]));

					productList.Add(phone);
				}
			
				dataReader.Close();
			}
			catch (InvalidOperationException e)
			{
				Console.WriteLine(e);
			}	
		}
	}
}